package cat.inspiracio.script;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/** Run with Java so that we can get our rhino. */
@SuppressWarnings("restriction")
public class RhinoTest extends AbstractTest{

	ScriptEngineManager manager=new ScriptEngineManager();
	private ScriptEngine engine;
	private ScriptEngineFactory factory;

	@Before public void before(){
		String version=version();

		//Java 1.7 already has its own "rhino" scripting engine.
		if(version.startsWith("1.7"))
			engine=manager.getEngineByName("rhino-nonjdk");
		
		//In Java 1.8 there is no "rhino" scripting engine, except for the one we add.
		else
			engine=manager.getEngineByName("rhino");
		
		factory=engine.getFactory();
	}
	
	@After public void after(){}
	
	@Test public void t(){
		assertEquals("rhino", factory.getParameter("javax.script.name"));
		assertEquals("Mozilla Rhino", factory.getEngineName());
		assertEquals("1.7.10", factory.getEngineVersion());
		assertEquals("ECMAScript", factory.getLanguageName());
		assertEquals("1.7", factory.getLanguageVersion());
		assertEquals(Arrays.asList("js"), factory.getExtensions());
		assertEquals(Arrays.asList("application/javascript", "application/ecmascript", "text/javascript", "text/ecmascript"), factory.getMimeTypes());
		assertEquals(Arrays.asList("rhino-nonjdk", "rhino"), factory.getNames());
		assertInstance(com.sun.phobos.script.javascript.RhinoScriptEngine.class, engine);
		assertInstance(com.sun.phobos.script.javascript.RhinoScriptEngineFactory.class, factory);
	}

	@Test public void nashorn(){
		ScriptEngine nashorn=manager.getEngineByName("nashorn");
		ScriptEngineFactory factory=nashorn.getFactory();
		assertEquals("Oracle Nashorn", factory.getEngineName());
	}

	@Test public void javascript(){
		ScriptEngine nashorn=manager.getEngineByName("javascript");
		ScriptEngineFactory factory=nashorn.getFactory();
		assertEquals("Oracle Nashorn", factory.getEngineName());
	}

}
