# rhino-js-engine

Takes Mozilla JavaScript engine _Rhino_ (version 1.7.7.1) and builds an
adapter for it so that it can be used in Java scripting.

## Why would I want to use Rhino 1.7.7.1?

Java already has a JavaScript engine.
In Java 7 it was a modified Rhino. 
In Java 8 it is a new engine called "Nashorn".

But maybe you have some technical reason why you prefer Mozilla's Rhino
which has some special functionalities that are not in Nashorn.

Or you may want to use Java's JavaScript in Google App Engine, 
where even the Nashorn engine is not included.

## How do I use it?

The code is a Maven project.

It is easy to change the included JavaScript engine from Rhino 1.7.10 to something else.

In your project, add rhino-js-engine as a dependency:

		<dependency>
		    <groupId>cat.inspiracio</groupId>
		    <artifactId>rhino-js-engine</artifactId>
		    <version>1.7.10</version>
		</dependency>

You don't need to add Rhino itself since it is pulled in by rhino-js-engine. 

In your java code, get the scripting engine called "rhino":

	import javax.script.*;
	
	ScriptEngineManager manager=new ScriptEngineManager();
	ScriptEngine engine=manager.getEngineByName("rhino");
	
Now you can call the script engine methods:

	engine.eval(program);

## engines provided

### RhinoScriptEngineFactory

It has these properties:   

* javax.script.name="rhino"
* javax.script.engine="Mozilla Rhino"
* javax.script.engine_version="1.7.10"
* javax.script.language="ECMAScript"
* javax.script.language_version="1.7"
* THREADING="MULTITHREADED"

It has these names:

* rhino-nonjdk
* rhino

It is associated wit these mimeTypes:

* application/javascript
* application/ecmascript
* text/javascript
* text/ecmascript

and these extensions:

* js

## EmbeddedRhinoScriptEngineFactory

It has these properties:   

* javax.script.name="embedded-javascript"
* javax.script.engine="Mozilla Rhino"
* javax.script.engine_version="1.7.10"
* javax.script.language="EmbeddedECMAScript"
* javax.script.language_version="1.7"
* THREADING="MULTITHREADED"

It has these names:

* ejs
* EmbeddedJavaScript
* embeddedjavascript

It is associated wit these mimeTypes:

* application/embeddedjavascript
* text/embeddedjavascript

and these extensions:

* ejs